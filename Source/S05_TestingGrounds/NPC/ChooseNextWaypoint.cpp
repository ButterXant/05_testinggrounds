// Fill out your copyright notice in the Description page of Project Settings.

#include "ChooseNextWaypoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "PatrolRoute.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) {
	// get the patrolling guard
	auto PatrollingGuard = OwnerComp.GetAIOwner()->GetPawn();
	if (!PatrollingGuard) return EBTNodeResult::Failed;

	// get the patrol route component
	auto PatrolRoute = PatrollingGuard->FindComponentByClass<UPatrolRoute>();
	if (!PatrolRoute) return EBTNodeResult::Failed;
	
	// get the patrol points
	auto PatrolPoints = PatrolRoute->GetPatrolPoints();
	if(PatrolPoints.Num() == 0) return EBTNodeResult::Failed;

	// get index of waypoint
	auto BlackboardComp = OwnerComp.GetBlackboardComponent();
	auto Index = BlackboardComp->GetValueAsInt(IndexKey.SelectedKeyName);

	// set new waypoint
	BlackboardComp->SetValueAsObject(WaypointKey.SelectedKeyName, PatrolPoints[Index]);

	// cycle index
	auto NextIndex = (Index + 1) % PatrolPoints.Num();
	BlackboardComp->SetValueAsInt(IndexKey.SelectedKeyName, NextIndex);

	// UE_LOG(LogTemp, Warning, TEXT("code reached lalalala"));
	return EBTNodeResult::Succeeded;
}


